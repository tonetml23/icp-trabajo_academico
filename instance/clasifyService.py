import rnet as rn
import boto3
import json
import os
import time

folder = "Proyecto_183_191/"
download_folder = "downloads"

sqs = boto3.resource('sqs')
s3 = boto3.client('s3')
sbd_client = boto3.client('sdb')

queue = sqs.get_queue_by_name(QueueName='cola-proyecto-183-191')

while True:
    # Process messages by printing out body and optional author name
    for message in queue.receive_messages(MessageAttributeNames=['Author']):
        # Get the custom author message attribute if it was set
        author_text = ''
        if message.message_attributes is not None:
            author_name = message.message_attributes.get('Author').get('StringValue')
            if author_name:
                author_text = ' ({0})'.format(author_name)


        body = json.loads(message.body)
        key = body["detail"]["requestParameters"]["key"]
        
        #Comprobar si el evento se ha generado en la carpeta correcta
        if folder not in key:
            print("El mensaje no viene de la carpeta correcta: " + key)
            continue
        else:
            print("Downloading: " + key)
        
        #Descargar imagen
        pos = key.rfind("/")
        localPath = download_folder + key[pos:]
        s3.download_file('alucloud-lambda', key, localPath)
        
        #Clasificar imagen
        tags = rn.clasify_image(localPath)
        
        #Insertar Item en la Base de Datos
        response = sbd_client.batch_put_attributes(
                DomainName='Imagenes',
                Items=[
                    {
                        'Name': key[pos:],
                        'Attributes': [
                            {
                                'Name': 'topic',
                                'Value': tags[0][0][1],
                                'Replace': True
                            },
                            {
                                'Name': 'accuracy',
                                'Value': str(tags[0][0][2]),
                                'Replace': True
                            }
                        ]
                    },
                ]
            )  

        print("Mensaje procesado: " + str(response))      

        
        # Let the queue know that the message is processed
        message.delete()

    time.sleep(0.1) #No tiene por que ser necesario, ya que se ha utilizado long polling en la cola
