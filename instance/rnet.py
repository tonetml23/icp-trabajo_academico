from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.utils import plot_model
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.resnet50 import preprocess_input
import numpy as np
from tensorflow.keras.applications.resnet50 import decode_predictions

def clasify_image(url):

    #Load the model
    model = ResNet50(
        include_top=True, weights='imagenet', input_tensor=None,
        input_shape=None, pooling=None, classes=1000
    )

    #Load image
    filePath = url
    image1 = image.load_img(filePath, target_size = (224, 224))

    #Convert image into array
    transformedImage = image.img_to_array(image1)

    #Expand the dimension
    transformedImage = np.expand_dims(transformedImage, axis = 0)

    #Preprocess the image
    transformedImage = preprocess_input(transformedImage)

    #Predict
    prediction = model.predict(transformedImage)

    #Decode and print the prediction
    predictionLabel = decode_predictions(prediction, top = 5)
    return predictionLabel

#print(clasify_image("cat.jpeg"))
