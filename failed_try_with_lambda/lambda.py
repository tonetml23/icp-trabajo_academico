from __future__ import print_function
import boto3
import os
import sys
import uuid
     
s3_client = boto3.client('s3')
sbd_client = boto3.client('sdb')
     
def handler(event, context):
    for record in event['Records']:
        bucket = "alucloud-lambda"#record['s3']['bucket']['name']
        key = record['s3']['object']['key'] 
        img_key = key
        img_path = '/tmp/{}{}'.format(uuid.uuid4(), key)
                
        print ("Downloading image in bucket {} with key {}".format(bucket,key))          
        os.makedirs(os.path.dirname(img_path), exist_ok = True)
        s3_client.download_file(bucket, key, img_path)
        
        # #Download libraries
        key = "Proyecto_183_191/lib/pylib.zip"
        download_path = '/tmp/' + key
        unzip_path = "/tmp/pylibs/"
        print ("Downloading image in bucket {} with key {}".format(bucket,key))          
        os.makedirs(os.path.dirname(download_path), exist_ok = True)
        s3_client.download_file(bucket, key, download_path)
        
        print("Se ha descargado. Haciendo unzip.")
        
        #unzip
        res = os.system("unzip " + download_path + " -d " + unzip_path)
        print(res)
        
        # import resnet
        tags = clasify_image(img_path)
        print(tags)
        
        
        #Insert item into SimpleDB
        response = sbd_client.batch_put_attributes(
            DomainName='Imagenes',
            Items=[
                {
                    'Name': 'Prueba',
                    'Attributes': [
                        {
                            'Name': 'URL',
                            'Value': img_key,
                            'Replace': True
                        },
                        {
                            'Name': 'topic',
                            'Value': tags[0][0][1],
                            'Replace': True
                        },
                        {
                            'Name': 'accuracy',
                            'Value': tags[0][0][2],
                            'Replace': True
                        }
                    ]
                },
            ]
        )        
if __name__ == '__main__':
  tag_image(sys.argv[1])
  
